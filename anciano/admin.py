from django.contrib import admin
from django import forms
from .models import *
from consulta.models import *

# Register your models here.

class ContactoInline(admin.StackedInline):
    model = Contacto
    extra = 1


class ConsultaInline(admin.StackedInline):
    model = Consulta
    extra = 1


class IngresoInline(admin.StackedInline):
    model = Ingreso
    extra = 1
    max_num = 1


class SalidaInline(admin.StackedInline):
    model = Salida
    extra = 1


class HospitalizacionInline(admin.StackedInline):
    model = Hospitalizacion
    extra = 1


class ActividadDiariaAdminModelForm(forms.ModelForm):
    levantarseAcostarse = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Levantarse / Acostarse")
    comidaBebida = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Comida / Bebida")
    vestirseDesvestirse = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Vestirse / Desvestirse")
    lavarseArreglarse = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Lavarse / Arreglarse")
    bañarseDucharse = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Bañarse / Ducharse")
    usoSanitarios = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Uso sanitarios")
    vision = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Visión")
    audicion = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Audición")
    orientacionTE = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Orientación Temporoesp.")
    desplazamientoVivienda = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Desplazamiento en vivienda")
    relacionEntorno = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Relación con el entorno")
    capacidadAutoproteccion = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Capacidad de autoprotección")
    capacidadInteraccion = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Capacidad de interacción")
    laboresHogar = forms.MultipleChoiceField(choices= ActividadDiaria.ACTIVIDADESVD, widget=forms.RadioSelect, label="Labores del hogar")

    class Meta:
        model = ActividadDiaria
        fields = "__all__"


class ActividadDiariaAdmin(admin.ModelAdmin):
    form = ActividadDiariaAdminModelForm


class ActividadDiariaInline(admin.StackedInline):
    model = ActividadDiaria
    form = ActividadDiariaAdminModelForm
    extra = 0
    max_num = 1

    class Media:
        css = {
            'all': ('ingreso/checkbox.css',)
        }

class AncianoAdmin(admin.ModelAdmin):
    inlines = [
        ContactoInline, IngresoInline, ActividadDiariaInline
    ]


admin.site.register(Anciano, AncianoAdmin)
admin.site.register(ActividadDiaria, ActividadDiariaAdmin)
admin.site.register(Ingreso)
admin.site.register(Contacto)
admin.site.register(ActividadFisica)
admin.site.register(Salida)
admin.site.register(Hospitalizacion)
admin.site.register(Parentesco)
