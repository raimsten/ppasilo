from django.db import models

# Create your models here.
class ObraSocial(models.Model):
    nombre = models.CharField(max_length=60, unique=True, verbose_name="Nombre de obra social")

    class Meta:
        verbose_name = "Obra social"
        verbose_name_plural = "Obras sociales"

    def __str__(self):
        return self.nombre
