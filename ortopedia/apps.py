from django.apps import AppConfig


class OrtopediaConfig(AppConfig):
    name = 'ortopedia'
