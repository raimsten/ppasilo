from django.db import models

class TipoProfesional(models.Model):
    descripcion = models.CharField(max_length=50, unique=True, verbose_name="Tipo de profesión")

    class Meta:
        verbose_name = "Tipo de profesional"
        verbose_name_plural = "Tipo de profesionales"

    def __str__(self):
        return self.descripcion


class Profesional(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    email = models.EmailField(max_length=255, blank=True, null= True)
    telefono = models.CharField(max_length=20, blank=True, null=True, verbose_name="Teléfono")
    matricula = models.CharField(max_length=10, blank=True, null=True, verbose_name="Matrícula")
    tipo = models.ForeignKey(TipoProfesional, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Profesional"
        verbose_name_plural = "Profesionales"

    def __str__(self):
        return self.tipo.__str__() + " - " + self.apellido + " " + self.nombre
