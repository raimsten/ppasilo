from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(TipoProfesional)
admin.site.register(Profesional)