### Credenciales

user = admin  
pass = asilo1234  

---
### Instalación
    apt-get install python3-venv

### Crear virtual y activarla
    python3 -m venv virtualHogar
    cd virtualHogar
    source bin/activate


### Clonar el repositorio
La URL la encuentran en la parte superior derecha, dice Clone.

    git clone https://USER@bitbucket.org/raimsten/ppasilo.git
    python3 -m pip install --upgrade pip


### Instalar requerimientos  
    cd ppasilo
    pip install -r requirements.txt


### Para salir de la virtual
    deactivate
