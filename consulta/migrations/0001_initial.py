# Generated by Django 2.0.9 on 2018-10-31 18:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('anciano', '0001_initial'),
        ('patologia', '0001_initial'),
        ('profesional', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Consulta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('descripcion', models.TextField(blank=True, null=True)),
                ('anciano', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='anciano.Anciano')),
                ('patologias', models.ManyToManyField(blank=True, to='patologia.Patologia')),
                ('profesional', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='profesional.Profesional')),
            ],
        ),
    ]
