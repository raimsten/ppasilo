from django.db import models

# Create your models here.
class Hospital(models.Model):
    nombre = models.CharField(max_length=140)
    direccion = models.CharField(max_length=140, verbose_name="Dirección")
    telefono = models.CharField(max_length=140, verbose_name="Teléfono")

    class Meta:
        verbose_name = "Hospital"
        verbose_name_plural = "Hospitales"

    def __str__(self):
        return self.nombre
