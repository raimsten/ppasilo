from django.db import models

# Create your models here.
class Patologia(models.Model):
    nombre = models.CharField(max_length=50, )
    codigo = models.CharField(max_length=10, unique=True, blank=True, null=True, verbose_name="Código")
    descripcion = models.TextField(max_length=600, blank=True, null=True, verbose_name="Descripción")

    def __str__(self):
        return self.nombre
